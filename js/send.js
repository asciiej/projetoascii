function addClass(id, classe) {
    var elemento = document.getElementById(id);
    var classes = elemento.className.split(' ');
    var getIndex = classes.indexOf(classe);
    
    if (getIndex === -1) {
        classes.push(classe);
        elemento.className = classes.join(' ');
    }
}

function delClass(id, classe) {
    var elemento = document.getElementById(id);
    var classes = elemento.className.split(' ');
    var getIndex = classes.indexOf(classe);
  
    if (getIndex > -1) {
      classes.splice(getIndex, 1);
    }
    elemento.className = classes.join(' ');
}

window.addEventListener("DOMContentLoaded", function() {

    let form = document.getElementById("contact-form"),
        button = document.getElementById("btn-send"),
        status = document.getElementById("status"),
        name = document.getElementById("name"),
        email = document.getElementById("email"),
        tel = document.getElementById("tel"),
        message = document.getElementById("message")

    function success() {
        form.reset();
        button.style = "display: none "
        status.innerHTML = '<div id="status__correct" class="correct__content"> Obrigado! </div>'
        addClass('status', 'correct')
        setTimeout(function() {
            window.location.href = "./index.html";
        }, 5000);
    }

    function error() {
        status.innerHTML = '<div id="status__error" class="error error__content"> Oops! Verifique os dados novamente. </div>'
    }

    form.addEventListener("submit", function(ev) {
        ev.preventDefault()
        let data = new FormData(form)
        if (!name.value || (!email.value && !tel.value) || !message.value) {
            error()
            return false
        }
        else {
            ajax(form.method, form.action, data, success, error)
        }
    })
})

function ajax(method, url, data, success, error) {
    var xhr = new XMLHttpRequest()
    xhr.open(method, url)
    xhr.setRequestHeader("Accept", "application/json")
    xhr.onreadystatechange = function() {
        if (xhr.readyState !== XMLHttpRequest.DONE) return
        if (xhr.status === 200) {
            success(xhr.response, xhr.responseType)
        } else {
            error(xhr.status, xhr.response, xhr.responseType)
        }
    }
    xhr.send(data)
}